package com.marcvit.bestbuysmartphones.provider;

import com.marcvit.bestbuysmartphones.converter.SmartphoneJsonConverter;
import com.marcvit.bestbuysmartphones.fixture.BestBuyFixture;
import com.marcvit.bestbuysmartphones.http.JsonHttpClient;
import com.marcvit.bestbuysmartphones.model.Smartphone;
import com.marcvit.bestbuysmartphones.model.SmartphoneImage;
import com.marcvit.bestbuysmartphones.receiver.SmartphonesReceiver;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BestBuySmartphonesProviderTest {

    private static final String URL = "https://api.bestbuy.com/v1/products(categoryPath.id=abcat0800000)?format=json&show=name,manufacturer,salePrice,image,thumbnailImage&pageSize=10&page=1&apiKey=83u9nksmnxd8jp7fayp427dr";
    private static final int FIRST_PAGE = 1;
    private static final int PAGE_SIZE = 10;
    private static final int INDEX_FIRST = 0;
    private static final int ONCE = 1;

    private JsonHttpClient mMockedClient;
    private SmartphonesReceiver mMockedReceiver;
    private SmartphoneJsonConverter mMockedConverter;
    private BestBuySmartphonesProvider mProvider;
    private JSONObject mJson;

    @Before
    public void initialize() throws JSONException {
        mJson = BestBuyFixture.validSmartphonesJsonObject();

        mMockedClient = mock(JsonHttpClient.class);
        mMockedReceiver = mock(SmartphonesReceiver.class);
        mMockedConverter = mock(SmartphoneJsonConverter.class);

        mProvider = new BestBuySmartphonesProvider(
                        mMockedClient,
                        mMockedReceiver,
                        mMockedConverter);
    }

    @Test
    public void shouldReturnAListOfSmartphonesWhenFindAllIsCalled() {
        ArrayList<Smartphone> expectedSmartphones = new ArrayList<Smartphone>(){{
            add(new Smartphone(
                    "One Product",
                    "One Manufacturer",
                    "9.99",
                    new SmartphoneImage(
                            "http://image.com/image.png",
                            "http://image.com/thumbnail.png"
                    )));
        }};

        when(mMockedClient.get(URL)).thenReturn(mJson);
        when(mMockedConverter.fromJson(mJson)).thenReturn(expectedSmartphones);

        ArrayList<Smartphone> smartphones = mProvider.findAll(FIRST_PAGE, PAGE_SIZE);

        Smartphone first = smartphones.get(INDEX_FIRST);
        Smartphone firstExpected = expectedSmartphones.get(INDEX_FIRST);

        verify(mMockedClient, times(ONCE)).get(URL);
        verify(mMockedConverter, times(ONCE)).fromJson(mJson);

        assertEquals("The sizes of the arrays should be equal",
                expectedSmartphones.size(),
                smartphones.size());

        assertSmartphonesAreEqual(firstExpected, first);
    }

    private void assertSmartphonesAreEqual(Smartphone expected, Smartphone actual){
        assertEquals("The names of the smartphones should be equal",
                expected.getName(),
                actual.getName());

        assertEquals("The manufacturers of the smartphones should be equal",
                expected.getManufacturer(),
                actual.getManufacturer());

        assertEquals("The prices of the smartphones should be equal",
                expected.getPrice(),
                actual.getPrice());

        assertEquals("The thumbnail urls of the smartphones should be equal",
                expected.getThumbnailUrl(),
                actual.getThumbnailUrl());

        assertEquals("The images urls of the smartphones should be equal",
                expected.getImageUrl(),
                actual.getImageUrl());
    }

    @Test
    public void shouldHandOverTheSmartphonesToTheReceiver(){
        ArrayList<Smartphone> smartphones = new ArrayList<>();

        mProvider.deliverToReceiver(smartphones);

        verify(mMockedReceiver, times(ONCE)).receiveSmartphones(smartphones);
    }
}
