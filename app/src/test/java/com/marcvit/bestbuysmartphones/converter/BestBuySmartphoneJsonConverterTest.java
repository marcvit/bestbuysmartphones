package com.marcvit.bestbuysmartphones.converter;

import com.marcvit.bestbuysmartphones.fixture.BestBuyFixture;
import com.marcvit.bestbuysmartphones.model.Smartphone;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class BestBuySmartphoneJsonConverterTest {

    private static final String NAME = "Acoustic Research - USB Wall Plate Charger - Multi";
    private static final String MANUFACTURER = "Acoustic Research";
    private static final String PRICE = "19.99";
    private static final String THUMBNAIL_URL = "http://images.bestbuy.com/BestBuy_US/images/products/4296/4296401_s.gif";
    private static final String IMAGE_URL = "http://images.bestbuy.com/BestBuy_US/images/products/4296/4296401_sa.jpg";

    private static final int INDEX_FIRST = 0;

    private JSONObject mJson = null;

    @Before
    public void initialize() throws JSONException {
        mJson = BestBuyFixture.validSmartphonesJsonObject();
    }

    @Test
    public void convertFromJsonToSmartphonesArray() throws JSONException {
        BestBuySmartphoneJsonConverter converter = new BestBuySmartphoneJsonConverter();

        ArrayList<Smartphone> smartphones = converter.fromJson(mJson);

        Smartphone firstSmartphone = smartphones.get(INDEX_FIRST);

        assertEquals("Names are not equal", NAME, firstSmartphone.getName());
        assertEquals("Manufacturers are not equal", MANUFACTURER, firstSmartphone.getManufacturer());
        assertEquals("Prices are not equal", PRICE, firstSmartphone.getPrice());
        assertEquals("Thumbnails urls are not equal", THUMBNAIL_URL, firstSmartphone.getThumbnailUrl());
        assertEquals("Images urls are not equal", IMAGE_URL, firstSmartphone.getImageUrl());
    }
}
