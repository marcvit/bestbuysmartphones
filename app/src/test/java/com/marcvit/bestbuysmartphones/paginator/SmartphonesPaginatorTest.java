package com.marcvit.bestbuysmartphones.paginator;

import com.marcvit.bestbuysmartphones.model.Smartphone;
import com.marcvit.bestbuysmartphones.provider.SmartphonesProvider;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SmartphonesPaginatorTest {

    private static final int SECOND_PAGE = 2;
    private static final int PAGE_SIZE = 10;
    private static final int ONCE = 1;

    private SmartphonesProvider mMockedProvider;
    private SmartphonesPaginator mPaginator;

    @Before
    public void initialize(){
        mMockedProvider = mock(SmartphonesProvider.class);
        mPaginator = new SmartphonesPaginator(mMockedProvider, PAGE_SIZE);
    }

    @Test
    public void shouldFetchNextPage(){
        ArrayList<Smartphone> smartphones = new ArrayList<>();

        when(mMockedProvider.findAll(SECOND_PAGE, PAGE_SIZE)).thenReturn(smartphones);

        mPaginator.fetchNextPage();

        verify(mMockedProvider, times(ONCE)).findAll(SECOND_PAGE, PAGE_SIZE);
        verify(mMockedProvider, times(ONCE)).deliverToReceiver(smartphones);
    }
}
