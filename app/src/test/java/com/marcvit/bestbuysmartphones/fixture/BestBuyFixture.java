package com.marcvit.bestbuysmartphones.fixture;

import org.json.JSONException;
import org.json.JSONObject;

public class BestBuyFixture {
    private static final String VALID_JSON = "{\"from\": 1,\"to\": 1,\"total\": 6521,\"currentPage\": 1,\"totalPages\": 6521,\"queryTime\": \"0.006\",\"totalTime\": \"0.011\",\"partial\": false,\"canonicalUrl\":\"/v1/products(categoryPath.id=abcat0800000)?show=name,manufacturer,salePrice,image,thumbnailImage&format=json&apiKey=83u9nksmnxd8jp7fayp427dr\",\"products\": [{\"name\":\"Acoustic Research - USB Wall Plate Charger - Multi\",\"manufacturer\": \"Acoustic Research\",\"salePrice\": \"19.99\",\"image\": \"http://images.bestbuy.com/BestBuy_US/images/products/4296/4296401_sa.jpg\",\"thumbnailImage\": \"http://images.bestbuy.com/BestBuy_US/images/products/4296/4296401_s.gif\"}]}";

    public static JSONObject validSmartphonesJsonObject() throws JSONException {
        return new JSONObject(VALID_JSON);
    }
}
