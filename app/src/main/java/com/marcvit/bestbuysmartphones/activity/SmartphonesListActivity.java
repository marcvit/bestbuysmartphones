package com.marcvit.bestbuysmartphones.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.marcvit.bestbuysmartphones.R;
import com.marcvit.bestbuysmartphones.adapter.SmartphonesAdapter;
import com.marcvit.bestbuysmartphones.factory.SmartphonesProviderFactory;
import com.marcvit.bestbuysmartphones.listener.SmartphonesScrollListener;
import com.marcvit.bestbuysmartphones.model.Smartphone;
import com.marcvit.bestbuysmartphones.paginator.SmartphonesPaginator;
import com.marcvit.bestbuysmartphones.provider.SmartphonesProvider;
import com.marcvit.bestbuysmartphones.receiver.SmartphonesReceiver;

import java.util.ArrayList;

public class SmartphonesListActivity extends AppCompatActivity implements SmartphonesReceiver {

    private static final int FIRST_PAGE = 1;
    private static final int PAGE_SIZE = 10;

    private SwipeRefreshLayout mSwipeLayout;
    private RecyclerView mSmartphonesRecyclerView;
    private SmartphonesAdapter mSmartphonesAdapter;
    private SmartphonesProvider mSmartphonesProvider;
    private SmartphonesPaginator mSmartphonesPaginator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smartphones_list);

        setupSmartphonesProvider();

        setupRefreshLayout();

        setupSmartphonesRecyclerView();
    }

    @Override
    protected void onStart(){
        super.onStart();

        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                loadSmartphones();
                return null;
            }
        }.execute();
    }

    private void setupSmartphonesProvider() {
        SmartphonesReceiver receiver = this;
        mSmartphonesProvider =
                SmartphonesProviderFactory.CreateBestBuyProviderWithReceiver(receiver);
    }

    private void setupRefreshLayout(){
        mSwipeLayout = (SwipeRefreshLayout)findViewById(R.id.swr_main);
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new AsyncTask<Void, Void, Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        refreshSmartphones();
                        return null;
                    }
                }.execute();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void setupSmartphonesRecyclerView(){

        mSmartphonesRecyclerView = (RecyclerView)findViewById(R.id.lst_smartphones);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mSmartphonesRecyclerView.setLayoutManager(layoutManager);

        mSmartphonesPaginator = new SmartphonesPaginator(mSmartphonesProvider, PAGE_SIZE);

        SmartphonesScrollListener scrollListener =
                new SmartphonesScrollListener(
                        mSmartphonesPaginator,
                        layoutManager);

        mSmartphonesRecyclerView.addOnScrollListener(scrollListener);
    }

    private void refreshSmartphones(){
        final ArrayList<Smartphone> smartphones = findAllSmartphones();

        synchronized (smartphones){
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSmartphonesPaginator.resetPagination();
                    mSmartphonesAdapter.updateSmartphonesWith(smartphones);
                    mSwipeLayout.setRefreshing(false);
                }
            });
        }
    }

    private void loadSmartphones(){
        final ArrayList<Smartphone> smartphones = findAllSmartphones();

        synchronized (smartphones){
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSmartphonesAdapter = new SmartphonesAdapter(smartphones);
                    mSmartphonesRecyclerView.setAdapter(mSmartphonesAdapter);
                }
            });
        }
    }

    private ArrayList<Smartphone> findAllSmartphones(){
        return mSmartphonesProvider.findAll(FIRST_PAGE, PAGE_SIZE);
    }

    @Override
    public void receiveSmartphones(final ArrayList<Smartphone> smartphones) {
        synchronized (smartphones){
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSmartphonesAdapter.addSmartphones(smartphones);
                }
            });
        }
    }
}