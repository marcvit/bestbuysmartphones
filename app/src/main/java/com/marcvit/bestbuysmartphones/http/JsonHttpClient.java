package com.marcvit.bestbuysmartphones.http;

import org.json.JSONObject;

public interface JsonHttpClient {
    JSONObject get(String url);
}