package com.marcvit.bestbuysmartphones.http;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class VolleyJsonHttpClient implements JsonHttpClient{

    private RequestQueue mQueue;

    public VolleyJsonHttpClient(Context context){
        mQueue = Volley.newRequestQueue(context);
    }

    @Override
    public JSONObject get(String url) {
        RequestFuture<JSONObject> future = RequestFuture.newFuture();

        JsonObjectRequest request = new JsonObjectRequest(url, new JSONObject(), future, future);

        future.setRequest(mQueue.add(request));

        JSONObject response = null;

        try{
            response = future.get(30, TimeUnit.SECONDS);
        }catch(InterruptedException e){
            e.printStackTrace();
            e.getCause().printStackTrace();
        }catch(ExecutionException e){
            e.printStackTrace();
            e.getCause().printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return response;
    }
}