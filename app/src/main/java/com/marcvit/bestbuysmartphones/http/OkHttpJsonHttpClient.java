package com.marcvit.bestbuysmartphones.http;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OkHttpJsonHttpClient implements JsonHttpClient {

    OkHttpClient mClient;

    public OkHttpJsonHttpClient(){
        mClient = new OkHttpClient();
    }

    @Override
    public JSONObject get(String url) {
        Request request = new Request.Builder()
                .url(url)
                .build();

        JSONObject jsonObjectResponse = null;

        try {
            Response response = mClient.newCall(request).execute();

            String jsonResponse = response.body().string();

            jsonObjectResponse = new JSONObject(jsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
            e.getCause().printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
            e.getCause().printStackTrace();
        }

        return jsonObjectResponse;
    }
}
