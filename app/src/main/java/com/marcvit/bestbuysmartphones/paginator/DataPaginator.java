package com.marcvit.bestbuysmartphones.paginator;

public interface DataPaginator {
    void fetchNextPage();

    int getPageSize();

    boolean isPaginating();

    void resetPagination();
}
