package com.marcvit.bestbuysmartphones.paginator;

import com.marcvit.bestbuysmartphones.model.Smartphone;
import com.marcvit.bestbuysmartphones.provider.SmartphonesProvider;

import java.util.ArrayList;

public class SmartphonesPaginator implements DataPaginator {

    private static final int FIRST_PAGE = 1;

    private int mCurrentPage;
    private SmartphonesProvider mProvider;
    private int mPageSize;
    private boolean mIsLoadingPage;

    public SmartphonesPaginator(SmartphonesProvider provider, int pageSize){
        mProvider = provider;
        mPageSize = pageSize;
        mIsLoadingPage = false;
        mCurrentPage = FIRST_PAGE;
    }

    @Override
    public void fetchNextPage() {
        if(mIsLoadingPage){
            return;
        }

        mIsLoadingPage = true;
        mCurrentPage++;

        ArrayList<Smartphone> smartphones = mProvider.findAll(mCurrentPage, mPageSize);
        mProvider.deliverToReceiver(smartphones);

        mIsLoadingPage = false;
    }

    @Override
    public int getPageSize(){
        return mPageSize;
    }

    @Override
    public boolean isPaginating(){
        return mIsLoadingPage;
    }

    @Override
    public void resetPagination(){
        mCurrentPage = FIRST_PAGE;
    }
}
