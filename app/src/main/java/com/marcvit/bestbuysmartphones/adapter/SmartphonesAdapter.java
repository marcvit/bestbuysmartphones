package com.marcvit.bestbuysmartphones.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marcvit.bestbuysmartphones.R;
import com.marcvit.bestbuysmartphones.model.Smartphone;
import com.marcvit.bestbuysmartphones.view.LoadingView;
import com.marcvit.bestbuysmartphones.view.SmartphoneView;

import java.util.ArrayList;

public class SmartphonesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int VIEW_TYPE_SMARTPHONE = 0;
    private static final int VIEW_TYPE_LOADING = 1;

    private ArrayList<Smartphone> mSmartphones;

    public SmartphonesAdapter(ArrayList<Smartphone> smartphones){
        mSmartphones = smartphones;
    }

    @Override
    public int getItemViewType(int position){
        final boolean isLastPosition = position == mSmartphones.size();

        if(isLastPosition){
            return VIEW_TYPE_LOADING;
        }

        return VIEW_TYPE_SMARTPHONE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == VIEW_TYPE_LOADING){
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.loading_smartphone, parent, false);

            return new LoadingView(view);
        }

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_smartphone, parent, false);

        return new SmartphoneView(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof LoadingView){
            ((LoadingView)holder).show();
            return;
        }

        ((SmartphoneView)holder).bind(mSmartphones.get(position));
    }

    @Override
    public int getItemCount() {
        return mSmartphones.size() + 1;
    }

    public void updateSmartphonesWith(ArrayList<Smartphone> smartphones) {
        mSmartphones = smartphones;
        notifyDataSetChanged();
    }

    public void addSmartphones(ArrayList<Smartphone> smartphones) {
        mSmartphones.addAll(smartphones);
        notifyDataSetChanged();
    }
}
