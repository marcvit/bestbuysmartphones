package com.marcvit.bestbuysmartphones.listener;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.marcvit.bestbuysmartphones.paginator.DataPaginator;

@TargetApi(Build.VERSION_CODES.M)
public class SmartphonesScrollListener extends RecyclerView.OnScrollListener {

    private DataPaginator mPaginator;
    private LinearLayoutManager mLayoutManager;

    public SmartphonesScrollListener(DataPaginator paginator, LinearLayoutManager layoutManager){
        mPaginator = paginator;
        mLayoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy){
        super.onScrolled(recyclerView, dx, dy);

        if(mPaginator.isPaginating()){
            return;
        }

        if(!isEndOfPage()){
            return;
        }

        paginate();
    }

    private boolean isEndOfPage(){
        int visibleItemCount = mLayoutManager.getChildCount();
        int totalItemCount = mLayoutManager.getItemCount();
        int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                && firstVisibleItemPosition >= 0
                && totalItemCount >= mPaginator.getPageSize()) {
            return true;
        }

        return false;
    }

    private void paginate(){
        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... params) {
                mPaginator.fetchNextPage();
                return null;
            }
        }.execute();
    }
}