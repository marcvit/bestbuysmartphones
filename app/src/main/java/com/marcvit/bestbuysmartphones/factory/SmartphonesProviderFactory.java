package com.marcvit.bestbuysmartphones.factory;

import com.marcvit.bestbuysmartphones.converter.BestBuySmartphoneJsonConverter;
import com.marcvit.bestbuysmartphones.http.OkHttpJsonHttpClient;
import com.marcvit.bestbuysmartphones.provider.BestBuySmartphonesProvider;
import com.marcvit.bestbuysmartphones.provider.SmartphonesProvider;
import com.marcvit.bestbuysmartphones.receiver.SmartphonesReceiver;

public class SmartphonesProviderFactory {
    public static SmartphonesProvider CreateBestBuyProviderWithReceiver(SmartphonesReceiver receiver){
        OkHttpJsonHttpClient httpClient = new OkHttpJsonHttpClient();

        BestBuySmartphoneJsonConverter converter = new BestBuySmartphoneJsonConverter();

        return new BestBuySmartphonesProvider(httpClient, receiver, converter);
    }
}
