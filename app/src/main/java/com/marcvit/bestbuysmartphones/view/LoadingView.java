package com.marcvit.bestbuysmartphones.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.marcvit.bestbuysmartphones.R;

public class LoadingView extends RecyclerView.ViewHolder {

    private View mView;
    private ProgressBar mProgressBar;

    public LoadingView(View view) {
        super(view);
        mView = view;
        mProgressBar = (ProgressBar)mView.findViewById(R.id.prg_loading);
    }

    public void show(){
        mProgressBar.setIndeterminate(true);
    }
}
