package com.marcvit.bestbuysmartphones.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.marcvit.bestbuysmartphones.R;
import com.marcvit.bestbuysmartphones.model.Smartphone;
import com.squareup.picasso.Picasso;

public class SmartphoneView extends RecyclerView.ViewHolder {

    private View mView;
    private TextView mName;
    private TextView mManufacturer;
    private TextView mPrice;
    private ImageView mThumbnail;

    public SmartphoneView(View itemView) {
        super(itemView);
        mView = itemView;
        mName = (TextView)mView.findViewById(R.id.lbl_name);
        mManufacturer = (TextView)mView.findViewById(R.id.lbl_manufacturer);
        mPrice = (TextView)mView.findViewById(R.id.lbl_price);
        mThumbnail = (ImageView)mView.findViewById(R.id.img_thumbnail);
    }

    public void bind(Smartphone smartphone){
        mName.setText(smartphone.getName());
        mManufacturer.setText(smartphone.getManufacturer());
        mPrice.setText(smartphone.getPrice());

        Picasso.with(mView.getContext())
                .load(smartphone.getThumbnailUrl())
                .into(mThumbnail);
    }
}
