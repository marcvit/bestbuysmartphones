package com.marcvit.bestbuysmartphones.provider;

import com.marcvit.bestbuysmartphones.converter.SmartphoneJsonConverter;
import com.marcvit.bestbuysmartphones.http.JsonHttpClient;
import com.marcvit.bestbuysmartphones.model.Smartphone;
import com.marcvit.bestbuysmartphones.receiver.SmartphonesReceiver;

import org.json.JSONObject;

import java.util.ArrayList;

public class BestBuySmartphonesProvider implements SmartphonesProvider {

    private static final String API_KEY = "83u9nksmnxd8jp7fayp427dr";
    private static final String BASE_URL = "https://api.bestbuy.com/v1/products(categoryPath.id=abcat0800000)?format=json&show=name,manufacturer,salePrice,image,thumbnailImage";
    private static final String URL_REQUEST_FORMAT = "%s&pageSize=%d&page=%d&apiKey=%s";

    private JsonHttpClient mClient;
    private SmartphonesReceiver mReceiver;
    private SmartphoneJsonConverter mConverter;

    public BestBuySmartphonesProvider(JsonHttpClient client,
                                      SmartphonesReceiver receiver,
                                      SmartphoneJsonConverter converter){
        mClient = client;
        mReceiver = receiver;
        mConverter = converter;
    }

    @Override
    public ArrayList<Smartphone> findAll(int page, int pageSize) {
        String url = mountUrl(page, pageSize);

        JSONObject json = mClient.get(url);

        return mConverter.fromJson(json);
    }

    private String mountUrl(int page, int pageSize){
        return String.format(URL_REQUEST_FORMAT, BASE_URL, pageSize, page, API_KEY);
    }

    @Override
    public void deliverToReceiver(ArrayList<Smartphone> smartphones){
        if(mReceiver == null){
            return;
        }

        mReceiver.receiveSmartphones(smartphones);
    }
}