package com.marcvit.bestbuysmartphones.provider;

import com.marcvit.bestbuysmartphones.model.Smartphone;
import java.util.ArrayList;

public interface SmartphonesProvider {
    ArrayList<Smartphone> findAll(int page, int pageSize);

    void deliverToReceiver(ArrayList<Smartphone> smartphones);
}
