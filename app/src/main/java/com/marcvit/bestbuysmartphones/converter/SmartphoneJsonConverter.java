package com.marcvit.bestbuysmartphones.converter;

import com.marcvit.bestbuysmartphones.model.Smartphone;

import org.json.JSONObject;

import java.util.ArrayList;

public interface SmartphoneJsonConverter {
    ArrayList<Smartphone> fromJson(JSONObject json);
}
