package com.marcvit.bestbuysmartphones.converter;

import com.marcvit.bestbuysmartphones.model.Smartphone;
import com.marcvit.bestbuysmartphones.model.SmartphoneImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BestBuySmartphoneJsonConverter implements SmartphoneJsonConverter {

    private static final String PRODUCTS_ROOT_KEY = "products";
    private static final String NAME_KEY = "name";
    private static final String MANUFACTURER_KEY = "manufacturer";
    private static final String PRICE_KEY = "salePrice";
    private static final String IMAGE_KEY = "image";
    private static final String THUMBNAIL_KEY = "thumbnailImage";

    @Override
    public ArrayList<Smartphone> fromJson(JSONObject json) {
        ArrayList<Smartphone> smartphones = new ArrayList<>();

        if(json == null){
            return smartphones;
        }

        try {
            JSONArray products = json.getJSONArray(PRODUCTS_ROOT_KEY);

            int len = products.length();

            JSONObject product = null;

            for(int i = 0; i < len; i++){
                product = products.getJSONObject(i);
                smartphones.add(
                        new Smartphone(
                                product.getString(NAME_KEY),
                                product.getString(MANUFACTURER_KEY),
                                product.getString(PRICE_KEY),
                                new SmartphoneImage(
                                        product.getString(IMAGE_KEY),
                                        product.getString(THUMBNAIL_KEY)
                                )
                        )
                );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return smartphones;
    }
}
