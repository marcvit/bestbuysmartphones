package com.marcvit.bestbuysmartphones.model;

public class SmartphoneImage {

    private String mImageUrl;
    private String mThumbnailUrl;

    public SmartphoneImage(String imageUrl, String thumbnailUrl){
        mImageUrl = imageUrl;
        mThumbnailUrl = thumbnailUrl;
    }

    public String getImageUrl(){
        return mImageUrl;
    }

    public String getThumbnailUrl(){
        return mThumbnailUrl;
    }
}
