package com.marcvit.bestbuysmartphones.model;

public class Smartphone {
    private String mName;
    private String mManufacturer;
    private String mPrice;
    private SmartphoneImage mImage;

    public Smartphone(String name, String manufacturer, String price, SmartphoneImage image){
        mName = name;
        mManufacturer = manufacturer;
        mPrice = price;
        mImage = image;
    }

    public String getName() {
        return mName;
    }

    public String getManufacturer() {
        return mManufacturer;
    }

    public String getPrice() {
        return mPrice;
    }

    public String getThumbnailUrl() {
        return mImage.getThumbnailUrl();
    }

    public String getImageUrl() {
        return mImage.getImageUrl();
    }
}