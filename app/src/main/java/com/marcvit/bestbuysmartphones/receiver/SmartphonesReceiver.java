package com.marcvit.bestbuysmartphones.receiver;

import com.marcvit.bestbuysmartphones.model.Smartphone;
import java.util.ArrayList;

public interface SmartphonesReceiver {
    void receiveSmartphones(ArrayList<Smartphone> smartphones);
}
