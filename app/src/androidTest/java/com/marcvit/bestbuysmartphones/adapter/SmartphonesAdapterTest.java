package com.marcvit.bestbuysmartphones.adapter;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.marcvit.bestbuysmartphones.activity.SmartphonesListActivity;
import com.marcvit.bestbuysmartphones.model.Smartphone;
import com.marcvit.bestbuysmartphones.model.SmartphoneImage;
import com.marcvit.bestbuysmartphones.view.LoadingView;
import com.marcvit.bestbuysmartphones.view.SmartphoneView;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class SmartphonesAdapterTest {

    private static final int VIEW_TYPE_SMARTPHONE = 0;
    private static final int VIEW_TYPE_LOADING = 1;
    private static final String EMPTY_STRING = "";

    private SmartphonesAdapter mAdapter;
    private ArrayList<Smartphone> mSmartphones;
    private ViewGroup mParent;

    @Rule
    public ActivityTestRule<SmartphonesListActivity> mActivityRule = new ActivityTestRule<>(
            SmartphonesListActivity.class);

    @Before
    public void initialize(){
        mSmartphones = new ArrayList<>(threeSmartphones());
        mAdapter = new SmartphonesAdapter(mSmartphones);
        mParent = new ViewGroup(mActivityRule.getActivity()) {
            @Override
            protected void onLayout(boolean changed, int l, int t, int r, int b) {
            }
        };
    }

    private ArrayList<Smartphone> threeSmartphones(){
        String textFormat = "Smartphone %d";
        String defaultPrice = "100";
        String name;
        String manufaturer;
        String thumbnailURL = EMPTY_STRING;
        String imageURL = EMPTY_STRING;

        ArrayList<Smartphone> smartphones = new ArrayList<>();
        for(int i = 1; i <= 3; i++){
            name = String.format(textFormat, i);
            manufaturer = String.format(textFormat, i);

            smartphones.add(
                    new Smartphone(
                            name,
                            manufaturer,
                            defaultPrice,
                            new SmartphoneImage(
                                    thumbnailURL,
                                    imageURL
                            )
                    )
            );
        }

        return smartphones;
    }

    @Test
    public void itemCountShouldReturnArrayLengthPlusOne(){
        int arrayLengthPlusOne = mSmartphones.size() + 1;

        assertEquals("Item count should be array length + 1, because of the loading view",
                arrayLengthPlusOne,
                mAdapter.getItemCount());
    }

    @Test
    public void getItemTypeShouldReturnSmartphoneViewType(){
        int firstPosition = 0;

        assertEquals("The view type should be 0 ('smartphone' row)",
                VIEW_TYPE_SMARTPHONE,
                mAdapter.getItemViewType(firstPosition));
    }

    @Test
    public void getItemTypeShouldReturnLoadingViewType(){
        int outOfBoundsPosition = mSmartphones.size();

        assertEquals("The view type should be 1 ('loading' row)",
                VIEW_TYPE_LOADING,
                mAdapter.getItemViewType(outOfBoundsPosition));
    }

    @Test
    public void creatingViewHolderShouldReturnViewOfTypeSmartphoneView(){
        RecyclerView.ViewHolder holder = mAdapter.onCreateViewHolder(mParent, VIEW_TYPE_SMARTPHONE);

        assertTrue("ViewHolder should be a view of type 'SmartphoneView'",
                holder instanceof SmartphoneView);
    }

    @Test
    public void creatingViewHolderShouldReturnViewOfTypeLoadingView(){
        RecyclerView.ViewHolder holder = mAdapter.onCreateViewHolder(mParent, VIEW_TYPE_LOADING);

        assertTrue("ViewHolder should be a view of type 'LoadingView'",
                holder instanceof LoadingView);
    }
}
