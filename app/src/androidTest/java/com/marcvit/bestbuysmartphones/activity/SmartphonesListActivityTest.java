package com.marcvit.bestbuysmartphones.activity;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.marcvit.bestbuysmartphones.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class SmartphonesListActivityTest {

    private static final String PRODUCT_NAME = "";

    @Rule
    public ActivityTestRule<SmartphonesListActivity> activityRule =
            new ActivityTestRule<>(SmartphonesListActivity.class);

//    @Test
//    public void selectAProductOnTheListAndOpenDetails(){
//        onData(withText(PRODUCT_NAME)).inAdapterView(ViewMatchers.withId(R.id.lst_smartphones)).perform(click());
//
//        onView(withId(R.id.txt_product_name)).check(matches(withText(PRODUCT_NAME)));
//    }
}