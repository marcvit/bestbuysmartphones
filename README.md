## BestBuy's Smartphones App

This is a test project, which fetches all the BestBuy's products in the 'Smartphone' category and displays them in a list.
It uses BestBuy's public API, and a bunch of common Android/Java libs, such as:

- OkHttp;
- Volley;
- Espresso;
- JUnit;
- Mockito;
- Picasso;

As of coding standards, I'm using (or tried to use, as much as possible) [Google Java Style](https://google.github.io/styleguide/javaguide.html) for general Java code, and Android's [Code Style for Contributors](https://source.android.com/source/code-style.html).